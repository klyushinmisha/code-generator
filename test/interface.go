package test

type TestInterface interface {
	Method1()
	Method2(a int, b int) error
	Method3(a int, b int) (error, error)
	Method4(a, b int) (error, error)
	Method5(x bool, a, b int, c string) (error, error)
}
