package test

import (
	"testing"
)

func Test_TemplateRenderer_GeneratedBuilderCompiles(t *testing.T) {
	inf := TestInterface(nil)
	NewBuilder(inf).
		Decorate(func(TestInterface) TestInterface { return inf }).
		Build()
}

func Test_TemplateRenderer_GeneratedDecoratorCompiles(t *testing.T) {
	inf := TestInterface(nil)
	NewBuilder(inf).
		Decorate(NewTestDecorator).
		Build()
}
