package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"text/template"

	"gitlab.com/klyushinmisha/code-generator/pkg"
)

var InterfaceName string
var TemplateType string
var Name string

func init() {
	flag.StringVar(&InterfaceName, "interface", "undefined", "Specifies input interface type")
	flag.StringVar(&TemplateType, "type", "undefined", "Specifies used generator type")
	flag.StringVar(&Name, "name", "undefined", "[Optional] Specifies name of generated item")
	flag.Parse()
}

func main() {
	ext, err := pkg.OptionsExtractorFactory{}.Create(TemplateType)
	if err != nil {
		fmt.Println(err)
		return
	}
	templates := map[string]string{
		"builder":   pkg.BuilderTemplate,
		"decorator": pkg.DecoratorTemplate,
	}
	sourceCode, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		fmt.Println(err)
		return
	}
	template := template.Must(template.New(TemplateType).Parse(templates[TemplateType]))
	generator, err := pkg.NewGenerator(template, string(sourceCode), ext.Extract(Name))
	if err != nil {
		fmt.Println(err)
		return
	}
	err = generator.FromInterface(InterfaceName, os.Stdout)
	if err != nil {
		fmt.Println(err)
		return
	}
}
