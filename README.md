# Code Generator

## Installation

```
go get gitlab.com/klyushinmisha/code-generator/cmd/code-generator
```

## Usage

1. Prepare input.

```bash
$ cat test/interface.go
package test

type User struct {
        Username string
}

type UserRepository interface {
        GetById(id int) (User, error)
}
```

2. Run the tool and check output.

```bash
$ cat test/interface.go | code-generator -interface UserRepository -type builder > test/builder.go
$ cat test/builder.go 
```
```golang
package test

type decoratorFactory func(UserRepository) UserRepository

type Builder struct {
        target UserRepository
}

func NewBuilder(target UserRepository) *Builder {
    return &Builder{target}
}

func (builder *Builder) Decorate(factory decoratorFactory) *Builder {
        builder.target = factory(builder.target)
        return builder
}

func (builder *Builder) Build() UserRepository {
        return builder.target
}
```

```bash
$ cat test/interface.go | code-generator -interface UserRepository -type decorator -name Logging > test/logging_decorator.go                                    
$ cat test/logging_decorator.go
```
```golang
package test

type LoggingDecorator struct {
        target UserRepository
}

func NewLoggingDecorator(target UserRepository) UserRepository {
    return LoggingDecorator{target}
}

func (decorator LoggingDecorator) GetById(id int) (User, error) {
    panic("implement me")
    return decorator.target.GetById(id)
}
```