package pkg

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io"
	"text/template"
)

type ParamMeta struct {
	Name string
	Type string
}

type ResultMeta struct {
	Type string
}

type MethodMeta struct {
	Name    string
	Params  []ParamMeta
	Results []ResultMeta
}

type InterfaceMeta struct {
	Name    string
	Methods []MethodMeta
}

type PackageMeta struct {
	Name string
}

type Meta struct {
	Package   PackageMeta
	Interface InterfaceMeta
	Options   interface{}
}

type DecoratorOptions struct {
	Name string
}

type TokExtractor string

func (ext TokExtractor) Extract(expr ast.Expr) string {
	start := expr.Pos() - 1
	end := expr.End() - 1
	return string(ext)[start:end]
}

type OptionsExtractor interface {
	Extract(args ...string) interface{}
}

type OptionsExtractorFactory struct{}

func (factory OptionsExtractorFactory) Create(extType string) (OptionsExtractor, error) {
	switch extType {
	case "builder":
		return BuilderOptionsExtractor{}, nil
	case "decorator":
		return DecoratorOptionsExtractor{}, nil
	default:
		return nil, fmt.Errorf("Unknown type: %s", extType)
	}
}

type BuilderOptionsExtractor struct{}

func (extractor BuilderOptionsExtractor) Extract(_ ...string) interface{} {
	return nil
}

type DecoratorOptionsExtractor struct{}

func (extractor DecoratorOptionsExtractor) Extract(args ...string) interface{} {
	return DecoratorOptions{
		Name: args[0],
	}
}

type Generator struct {
	template        *template.Template
	pkgMetaSelector PackageMetaSelector
	infMetaSelector InterfaceMetaSelector
	options         interface{}
}

func NewGenerator(template *template.Template, sourceCode string, options interface{}) (*Generator, error) {
	generator := new(Generator)
	generator.template = template
	selector, err := NewAstSelector(string(sourceCode))
	if err != nil {
		return nil, err
	}
	generator.pkgMetaSelector = AstPackageSelector(selector)
	generator.infMetaSelector = AstInterfaceSelector(selector)
	generator.options = options
	return generator, nil
}

func (generator *Generator) FromInterface(interfaceName string, output io.Writer) error {
	infMeta, err := generator.infMetaSelector.Select(interfaceName)
	if err != nil {
		return err
	}
	meta := Meta{
		Package:   generator.pkgMetaSelector.Select(),
		Interface: infMeta,
		Options:   generator.options,
	}
	generator.template.Execute(output, meta)
	return nil
}

type PackageMetaSelector interface {
	Select() PackageMeta
}

type InterfaceMetaSelector interface {
	Select(interfaceName string) (InterfaceMeta, error)
}

type AstSelector struct {
	file         *ast.File
	tokExtractor TokExtractor
}

func NewAstSelector(sourceCode string) (AstSelector, error) {
	file, err := parser.ParseFile(token.NewFileSet(), "", sourceCode, 0)
	if err != nil {
		return AstSelector{}, err
	}
	return AstSelector{file, TokExtractor(sourceCode)}, nil
}

type AstInterfaceSelector AstSelector

func (selector AstInterfaceSelector) Select(interfaceName string) (InterfaceMeta, error) {
	for _, decl := range selector.file.Decls {
		switch decl := decl.(type) {
		case *ast.GenDecl:
			for _, spec := range decl.Specs {
				typ, ok := spec.(*ast.TypeSpec)
				if !ok {
					continue
				}
				inf, ok := typ.Type.(*ast.InterfaceType)
				if !ok {
					continue
				}
				if typ.Name.Name != interfaceName {
					continue
				}
				infMeta := InterfaceMeta{
					Name:    typ.Name.Name,
					Methods: []MethodMeta{},
				}
				for _, method := range inf.Methods.List {
					methodMeta := MethodMeta{}
					methodMeta.Name = method.Names[0].Name
					fun, _ := method.Type.(*ast.FuncType)

					methodMeta.Params = []ParamMeta{}
					for _, param := range fun.Params.List {
						if len(param.Names) == 0 {
							return infMeta, fmt.Errorf("Missing name for required interface method argument")
						}
						for _, arg := range param.Names {
							methodMeta.Params = append(methodMeta.Params, ParamMeta{
								Name: arg.Name,
								Type: selector.tokExtractor.Extract(param.Type),
							})
						}
					}

					methodMeta.Results = []ResultMeta{}
					if fun.Results != nil {
						for _, result := range fun.Results.List {
							methodMeta.Results = append(methodMeta.Results, ResultMeta{
								Type: selector.tokExtractor.Extract(result.Type),
							})
						}
					}
					infMeta.Methods = append(infMeta.Methods, methodMeta)
				}
				return infMeta, nil
			}
		}
	}
	return InterfaceMeta{}, fmt.Errorf("Interface not found")
}

type AstPackageSelector AstSelector

func (selector AstPackageSelector) Select() PackageMeta {
	pkgMeta := PackageMeta{Name: selector.file.Name.Name}
	return pkgMeta
}
