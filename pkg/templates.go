package pkg

const BuilderTemplate = `package {{.Package.Name}}

type decoratorFactory func({{.Interface.Name}}) {{.Interface.Name}}

type Builder struct {
	target {{.Interface.Name}}
}

func NewBuilder(target {{.Interface.Name}}) *Builder {
    return &Builder{target}
}

func (builder *Builder) Decorate(factory decoratorFactory) *Builder {
	builder.target = factory(builder.target)
	return builder
}

func (builder *Builder) Build() {{.Interface.Name}} {
	return builder.target
}`

const DecoratorTemplate = `package {{.Package.Name}}

type {{.Options.Name}}Decorator struct {
	target {{.Interface.Name}}
}

func New{{.Options.Name}}Decorator(target {{.Interface.Name}}) {{.Interface.Name}} {
    return {{.Options.Name}}Decorator{target}
}
{{- $dec := .Options.Name}}
{{range $method := .Interface.Methods}}
func (decorator {{$dec}}Decorator) {{$method.Name -}}(
    {{- range $index, $param := $method.Params -}}
        {{- if $index}}, {{end -}}
        {{- $param.Name}} {{$param.Type -}}
    {{- end -}}
{{- ")"}} {{"(" -}}
    {{range $index, $result := $method.Results -}}
        {{- if $index}}, {{end -}}
        {{- $result.Type -}}
    {{- end}}
{{- ")"}} {
    panic("implement me")
    {{if $method.Results}}return {{end -}}
    decorator.target.{{$method.Name}}(
        {{- range $index, $param := $method.Params -}}
        {{- if $index}}, {{end -}}
        {{- $param.Name}}{{- end -}}
    )
}
{{end}}`
