.PHONY: build install uninstall test clean

OS := $(or $(OS),linux)
ARCH := $(or $(ARCH),amd64)
BUILD_FLAGS := CGO_ENABLED=0 GOOS=$(OS) GOARCH=$(ARCH)
ifeq ($(OS),windows)
  EXTENSION := .exe
else
  EXTENSION := 
endif
APP_WITH_EXTENSION := code-generator$(EXTENSION)
BUILD_PATH := artifacts/$(OS)/$(ARCH)

build:
	${BUILD_FLAGS} go build -o ${BUILD_PATH}/${APP_WITH_EXTENSION} cmd/code-generator/main.go

install: build
	cp ${BUILD_PATH}/${APP_WITH_EXTENSION} ${GOPATH}/bin/${APP_WITH_EXTENSION}

uninstall:
	rm ${GOPATH}/bin/${APP_WITH_EXTENSION}

test:
	cat test/interface.go | code-generator -interface TestInterface -type builder > test/builder.go
	cat test/interface.go | code-generator -interface TestInterface -type decorator -name Test > test/decorator.go
	go test -timeout 30s ./... -v
	rm test/builder.go test/decorator.go

clean:
	rm -rf artifacts
	rm test/builder.go test/decorator.go
